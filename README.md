# Savory Quick Start

This is a quick start repo - Clone and start hacking with Savory! 

# Get started

To get started, first you need to install
[wasm-pack](https://github.com/rustwasm/wasm-pack), then simply run:
``` shell
git clone https://gitlab.com/MAlrusayni/savory-quick-start
cd savory-quick-start/savory-app
sh build.sh & sh serve.sh
```

This will do:
- Clone this repo
- Build the frontend and delpoy it to the server folder
- Run the server to serve our frontend (savory-app)

# Repo Structural

This repo contains two projects:
- Server (backend)
- Savory App (frontend)

Each have it's own directory, and they both are Rust projects. The server is
just a template that will only serve `server/index.html` file and the directory
`server/app` where we keep our wasm app.

Here is a full tree for this repo, so you have an overview:

``` shell
.
├── README.md
├── savory-app
│   ├── build.sh      <!-- This script will build our savory-app and put it in the server folder -->
│   ├── serve.sh      <!-- This script will run the server -->
│   ├── Cargo.toml
│   ├── Cargo.lock
│   └── src
│       └── lib.rs    <!-- Savory app srouce code -->
└── server
    ├── app           <!-- Here is where our server keep our app files wasm/json .. -->
    │   ├── package.json
    │   ├── savory-app.js
    │   └── savory-app_bg.wasm
    ├── Cargo.toml
    ├── Cargo.lock
    ├── index.html    <!-- This is the inhdex file we serve that will lode our app -->
    └── src
        └── main.rs   <!-- Server source code -->
```


# Workflow

Here is the expected workflow:

1) Run the server with `sh serve.sh` (you have to be in `savory-app/` dir)
2) Do codeing in `savory-app/src`
3) Build your frontend with `sh build.sh`
4) goto step 2
