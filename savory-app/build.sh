#!/usr/bin/env bash

wasm-pack build --target web --out-dir ../server/app --out-name savory-app --no-typescript
